<%@ Page Language="C#" MasterPageFile="~/AFD.master" AutoEventWireup="true" CodeFile="HelpPage.aspx.cs" Inherits="HelpPage" Title="Untitled Page" %>
<asp:Content ID="Content1" ContentPlaceHolderID="main" Runat="Server">
    Please direct your questions and service requests to the help-desk for AirForceAds.com<br />
    <br />
    <a href="mailto:airforcewebsite@ideacity.com" title="mailto:airforcewebsite@ideacity.com">
        <span style="color: #ffffff">airforcewebsite@ideacity.com</span></a>
</asp:Content>

