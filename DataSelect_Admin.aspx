<%@ Page Language="C#"  AutoEventWireup="true" CodeFile="DataSelect_Admin.aspx.cs" Inherits="DataSelect" Title="AF Data Selection Page" MasterPageFile="AFD.master" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>


<asp:Content ID="Content1" ContentPlaceHolderID="main" Runat="Server">
 
    <form runat="server" >
<telerik:RadScriptManager ID="RadScriptManager1" runat="server">
         </telerik:RadScriptManager>
    <telerik:RadFormDecorator ID="RadFormDecorator1" runat="server" DecoratedControls="Buttons, Scrollbars, Textbox, Label, H4H5H6"
        Skin="Black" />
                <div style="font-size: small; color: #CCCCCC; width: 750px; font-family: Verdana;">
                    Use this page to select and download your data. Visit each tab below to define 
                    your selection. On the fourth tab get a count and download data, or use the 
                    cancel button and reset the selection.
                    <br />
                    <br />
                    Use this button to reset the form</div>
<asp:Button ID="btnReset" runat="server" onclick="btnReset_Click" 
    Text="Reset All" Width="95px" />
<br />
         
<br />
<br />
<telerik:RadTabStrip ID="RadTabStrip1" runat="server" 
    MultiPageID="RadMultiPage1" SelectedIndex="0" Skin="Black" Height="25px" 
    OnTabClick="RadTabStrip1_TabClick" >
    <Tabs>
        
        <telerik:RadTab runat="server" PageViewID="pvLists" Text="1. Choose a List"  
            TabIndex="0" Value="Lists" ToolTip="Select a mailing list" 
            Width="170px" Selected="True" >
        </telerik:RadTab>
        <telerik:RadTab runat="server" PageViewID="pvGeo" Text="2. Select Geography" 
            TabIndex="1" Value="Geo" Width="170px">
        </telerik:RadTab>
        <telerik:RadTab runat="server" PageViewID="pvFilters" Text="3. Other Filters"  
            TabIndex="2" Value="Filters" Width="170px">
        </telerik:RadTab>
        <telerik:RadTab runat="server" PageViewID="pvCount" Text="4. Get Count and Data" 
            TabIndex="3" Value="Count" Width="170px">
        </telerik:RadTab>
    </Tabs>
</telerik:RadTabStrip>
<telerik:RadMultiPage ID="RadMultiPage1" Runat="server" SelectedIndex="2" 
    BorderColor="Silver" BorderStyle="Solid" BorderWidth="1px" Width="750px" >
       
     <telerik:RadPageView ID="pvLists" runat="server" Width="750px" 
         Selected="True">
         <table width="100%">
            <tr>
                <td>
                    <telerik:RadTreeView ID="tvLists" runat="server" BorderColor="Gray" 
                        BorderStyle="Inset" BorderWidth="1px" Skin="Black" Width="400px">
                        <Nodes>

                            <telerik:RadTreeNode runat="server" Text="Medical" Value="Medical" Visible="true" Expanded="true" Enabled="false">
                            <Nodes>
                                <telerik:RadTreeNode runat="server" Expanded="True" Text="MCAT" Value="All_MCAT">
                                    <Nodes>
                                        <telerik:RadTreeNode runat="server" Text="MCAT - Jul 2019" Value="MCAT - Jul 2019"></telerik:RadTreeNode>
                                        <telerik:RadTreeNode runat="server" Text="MCAT - Jun 2019" Value="MCAT - Jun 2019"></telerik:RadTreeNode>
                                        <telerik:RadTreeNode runat="server" Text="MCAT - Apr 2019" Value="MCAT - Apr 2019"></telerik:RadTreeNode>
                                        <telerik:RadTreeNode runat="server" Text="MCAT - Mar 2019" Value="MCAT - Mar 2019"></telerik:RadTreeNode>
                                        <telerik:RadTreeNode runat="server" Text="MCAT - Feb 2019" Value="MCAT - Feb 2019"></telerik:RadTreeNode>
                                        <telerik:RadTreeNode runat="server" Text="MCAT - Jan 2019" Value="MCAT - Jan 2019"></telerik:RadTreeNode> 
                                        <telerik:RadTreeNode runat="server" Text="MCAT - Sep 2018" Value="MCAT - Sep 2018"></telerik:RadTreeNode> 
                                        <telerik:RadTreeNode runat="server" Text="MCAT - Aug 2018" Value="MCAT - Aug 2018"></telerik:RadTreeNode>
                                        <telerik:RadTreeNode runat="server" Text="MCAT - Jul 2018" Value="MCAT - Jul 2018"></telerik:RadTreeNode>
                                        <telerik:RadTreeNode runat="server" Text="MCAT - Jun 2018" Value="MCAT - Jun 2018"></telerik:RadTreeNode>
                                        <telerik:RadTreeNode runat="server" Text="MCAT - Apr 2018" Value="MCAT - Apr 2018"></telerik:RadTreeNode>
                                        <telerik:RadTreeNode runat="server" Text="MCAT - Mar 2018" Value="MCAT - Mar 2018"></telerik:RadTreeNode>               
                                    </Nodes>
                                </telerik:RadTreeNode>
                                <telerik:RadTreeNode runat="server" Expanded="True" Text="All DAT" Value="All_DAT">
                                    <Nodes>
                                        <telerik:RadTreeNode runat="server" Text="DAT - Jul 2019" Value="DAT - Jul 2019"></telerik:RadTreeNode>	
                                        <telerik:RadTreeNode runat="server" Text="DAT - May 2019" Value="DAT - May 2019"></telerik:RadTreeNode>	
                                        <telerik:RadTreeNode runat="server" Text="DAT - Jun 2018" Value="DAT - Jun 2018"></telerik:RadTreeNode>	
                                        
                                    </Nodes>
                                </telerik:RadTreeNode>
                            </Nodes>
                            </telerik:RadTreeNode>	

							<telerik:RadTreeNode runat="server" Text="All Enlisted" Value="Enlisted" Expanded="True" enabled="false" >
                                <Nodes>
                                   <telerik:RadTreeNode runat="server" Text="All Special Ops" Value="All_SpecialOps">
                                      <Nodes>
                                        <telerik:RadTreeNode runat="server" Text="Special Ops ExactData" Value="SpecialOps ExactData"></telerik:RadTreeNode>
                                        <telerik:RadTreeNode runat="server" Text="Special Ops NRCCUA" Value="SpecialOps NRCCUA"></telerik:RadTreeNode>
                                        <telerik:RadTreeNode runat="server" Text="Special Ops Scholarships" Value="SpecialOps Scholarships"></telerik:RadTreeNode>
                                      </Nodes>
                                   </telerik:RadTreeNode>

                                  <telerik:RadTreeNode runat="server" Text="All STEM" Value="All_STEM">
                                      <Nodes>
                                        <telerik:RadTreeNode runat="server" Text="STEM ExactData" Value="STEM ExactData"></telerik:RadTreeNode>
                                        <telerik:RadTreeNode runat="server" Text="STEM Engineering NRCCUA" Value="STEMEngineering NRCCUA"></telerik:RadTreeNode>
                                        <telerik:RadTreeNode runat="server" Text="STEM InfoSystems NRCCUA" Value="STEMInfoSystems NRCCUA"></telerik:RadTreeNode>
                                        <telerik:RadTreeNode runat="server" Text="STEM Science NRCCUA" Value="STEMScience NRCCUA"></telerik:RadTreeNode>
                                        <telerik:RadTreeNode runat="server" Text="STEM Engineering Scholarships" Value="STEMEngineering Scholarships"></telerik:RadTreeNode>
                                        <telerik:RadTreeNode runat="server" Text="STEM InfoSystems Scholarships" Value="STEMInfoSystems Scholarships"></telerik:RadTreeNode>
                                        <telerik:RadTreeNode runat="server" Text="STEM Science Scholarships" Value="STEMScience Scholarships"></telerik:RadTreeNode>
                                       </Nodes>
                                   </telerik:RadTreeNode>
                                  <telerik:RadTreeNode runat="server" Text="All Aviation" Value="All_Aviation">
                                      <Nodes>
									    <telerik:RadTreeNode runat="server" Text="Aviation NRCCUA" Value="Aviation NRCCUA"></telerik:RadTreeNode>
                                        <telerik:RadTreeNode runat="server" Text="Aviation Scholarships" Value="Aviation Scholarships"></telerik:RadTreeNode>
                                       </Nodes>
                                   </telerik:RadTreeNode>

                                  <telerik:RadTreeNode runat="server" Text="All Community College" Value="All_CommunityCollege">
                                      <Nodes>
									    <telerik:RadTreeNode runat="server" Text="Community College NRCCUA" Value="School CommunityCollege NRCCUA"></telerik:RadTreeNode>
                                        <telerik:RadTreeNode runat="server" Text="Community College Scholarships" Value="School CommunityCollege Scholarships"></telerik:RadTreeNode>
                                       </Nodes>
                                   </telerik:RadTreeNode>
                                  <telerik:RadTreeNode runat="server" Text="All Drop-Outs" Value="All_DropOuts">
                                      <Nodes>
									    <telerik:RadTreeNode runat="server" Text="DropOuts0-2 Scholarships" Value="DropOuts0-2 Scholarships"></telerik:RadTreeNode>
                                        <telerik:RadTreeNode runat="server" Text="DropOuts21-25 Scholarships" Value="DropOuts21-25 Scholarships"></telerik:RadTreeNode>
                                      </Nodes>
                                   </telerik:RadTreeNode>
                                   <telerik:RadTreeNode runat="server" Text="All Vocational/TradeSchool" Value="All_TradeSchools">
                                      <Nodes>
									    <telerik:RadTreeNode runat="server" Text="Vocational/TradeSchool Scholarships" Value="School VocationalTradeSchool Scholarships"></telerik:RadTreeNode>
									</Nodes>
                                   </telerik:RadTreeNode>

								</Nodes>
                            </telerik:RadTreeNode>
                            
                           
						</Nodes>
                    </telerik:RadTreeView>
                </td>
                  <td valign="top">
                      <div align="center" class="right" 
                          style="padding: 15px; color: #FFFFFF; font-size: small;">
                          Select the desired list
                          <br />
                          <br />
                          Press the &quot;+&quot;&nbsp; to expand the category and see the specific lists/vendors 
                          available.</div>
                </td>

            
            </tr>
         </table>
     
         <div class="right" style="float: right; clear: both">
         <asp:Button ID="btnNext1" runat="server" Height="25px" onclick="btnNext1_Click" 
                 Text="Go To Next Step" ToolTip="Go to the Geography tab" Width="165px" />
         </div>
         <br />
         <br />
     </telerik:RadPageView>  
          
     <telerik:RadPageView ID="pvGeo" runat="server" Width="750px">
         <telerik:RadTreeView ID="tvGeography" runat="server" BorderColor="DarkGray" 
             BorderStyle="Inset" BorderWidth="1px" CheckBoxes="True" 
             DataFieldID="StateAbbrev" DataFieldParentID="Parent" DataMember="DefaultView" 
             DataSourceID="sqlGeography" DataTextField="StateName" 
             DataValueField="StateAbbrev" Skin="Black" Width="400px"></telerik:RadTreeView>
         <div class="right">
             <asp:Button ID="btnNext2" runat="server" Height="25px" onclick="btnNext2_Click" 
                 Text="Go To Next Step" ToolTip="Go to the Filters tab" Width="165px" />
         </div>
         <br />
     </telerik:RadPageView>
     
     <telerik:RadPageView ID="pvFilters" runat="server" Height="650px" 
         Width="750px" ><table style="width: 100%; height: 152px;"><tr>
             <td style="color: #FFFFFF; font-size: small; padding-right: 10px; height: 44px; padding-left: 0px;" 
                     valign="top" colspan="3">Make additional selections below or <i>leave 
                 unselected (blank) to get all records</i>.</td></tr><tr>
             <td style="width: 160px; color: #FFFFFF; font-size: small; padding-right: 10px; height: 135px;" 
                 valign="top">Limit to where specified addresses are present.<br />(Uncheck to get all)</td>
             <td style="height: 135px" valign="top"><asp:CheckBox ID="chkHomeAddress" runat="server" Font-Size="Small" ForeColor="White" 
                     Text="Only Where HOME address is present" />
                     <br />
                     <asp:CheckBox ID="chkWorkAddress" runat="server" Font-Size="Small" ForeColor="White" 
                     Text="Only where WORK address is present" /><br /><asp:CheckBox 
                     ID="chkEmailAddress" runat="server" Font-Size="Small" ForeColor="White" 
                     Text="Only where EMAIL address is present" /></td>
                <td style="height: 135px"></td>
                 
            </tr>
            <tr>
                <td style="width: 160px; height: 105px; font-size: small; color: #FFFFFF; padding-left: 0px; padding-right: 10px;" 
                     valign="top">Limit to a specific gender</td><td style="height: 105px" 
                    valign="top">
                    <asp:RadioButton ID="rbGender_F" runat="server" Font-Size="Small" 
                        ForeColor="White" GroupName="GenderButtons" Text="Only Females" />
                    <br />
                    <asp:RadioButton ID="rbGender_m" runat="server" Font-Size="Small" 
                        ForeColor="White" GroupName="GenderButtons" Text="Only Males" />
                    <br />
                    <asp:RadioButton ID="rbGender_b" runat="server" Checked="True" 
                        Font-Size="Small" ForeColor="White" GroupName="GenderButtons" 
                        Text="Both Males and Females" />
                </td><td style="height: 105px"></td></tr>
         <tr>
             <td style="width: 160px; height: 91px; font-size: small; color: #FFFFFF; padding-left: 0px; padding-right: 10px;" 
                 valign="top">
                 Limit to specified medical specialties<br />
                 (Uncheck all to get the full list)</td>
             <td style="height: 91px" valign="top">
                 <asp:Label ID="lblSpecialties" runat="server" Font-Bold="True" 
                     Font-Size="Small" ForeColor="White" Text="Specialties For: "></asp:Label>
                 <br />
                 <br />
                 <telerik:RadListBox ID="lstSpecialty" runat="server" CheckBoxes="True" 
                     EmptyMessage="(No Specialties)" SelectionMode="Multiple" Skin="Black" 
                     Width="300px">
                     <Items>
                         <%--<telerik:RadListBoxItem runat="server" Text="All Nurses" Value="N0" Checked="true" />--%>
                         <telerik:RadListBoxItem runat="server" AllowDrag="False" 
                             Text="Clinical/Medical/Surgical Care Nurse" Value="N1" />
                         <telerik:RadListBoxItem runat="server" Text="Mental Health Nurse" Value="N2" />
                         <telerik:RadListBoxItem runat="server" Text="Emergency/Critical Nurse" Value="N3" />
                         <telerik:RadListBoxItem runat="server" Text="Family Care Nurse" Value="N4" />

                     </Items>
                     <EmptyMessageTemplate>
                         No Specialties
                     </EmptyMessageTemplate>
                 </telerik:RadListBox>
             </td>
             <td style="height: 91px">
             </td>
         </tr>
         <tr><td style="width: 160px">&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr></table>
         <div class="right">
             <asp:Button ID="btnNext3" runat="server" Height="25px" onclick="btnNext3_Click" 
                 Text="Go To Next Step" ToolTip="Go to the Count and Data tab" Width="165px" />
         </div>
         <br />
         <br />
     </telerik:RadPageView>
    
     <telerik:RadPageView ID="pvCount" runat="server" Height="500px" Width="398px"><table><tr>
         <td align="right" style="width: 368px" valign="top">&nbsp;</td>
         <td style="width: 174px">
             &nbsp;</td>
         </tr>
         <tr>
             <td align="left" 
                 style="width: 368px; height: 62px; color: #FFFFFF; font-size: small;" 
                 valign="top">
                 Before you can get the data you need to confirm the record count.</td>
             <td align="center" style="width: 174px; height: 62px">
                 <asp:Button ID="btnGetCount" runat="server" OnClick="btnGetCount_Click" 
                     Text="Get the Record Count" Width="160px" />
             </td>
         </tr>
         <tr>
             <td align="center" rowspan="1" style="width: 368px" valign="baseline">
                 <asp:Panel ID="pnlNoList" runat="server" Font-Bold="True" ForeColor="#FF8080" 
                     HorizontalAlign="Center" Visible="False" Width="400px">
                     No List Selected!
                     <br />
                     Please select a list.<br />
                     <br />
                     <asp:Button ID="btnReturnToListsTab" runat="server" 
                         onclick="btnReturnToListsTab_Click" Text="Return to the Lists tab" 
                         Width="154px" />
                 </asp:Panel>
                 <br />
                 <asp:Panel ID="pnlNoGeo" runat="server" Font-Bold="True" ForeColor="#FF8080" 
                     HorizontalAlign="Center" Visible="False" Width="350px">
                     No Geography Selected!<br />
                     Please make a selection and try again.<br />
                     <br />
                     <asp:Button ID="btnReturnToGeoTab" runat="server" 
                         onclick="btnReturnToGeoTab_Click" Text="Return to the Geo tab" Width="154px" />
                     <br />
                 </asp:Panel>
                 <asp:Panel ID="pnlNoCount" runat="server" Font-Bold="True" ForeColor="#FF8080" 
                     HorizontalAlign="Center" Visible="False" Width="400px">
                     These selections will return no records!<br />
                     Please alter your selection and try again.<br />
                     <br />
                     <asp:Button ID="Button1" runat="server" onclick="Button1_Click" 
                         Text="GoTo Geo tab" Width="120px" />
                     &nbsp;
                     <asp:Button ID="btnGotoFiltersTab" runat="server" 
                         onclick="btnGotoFiltersTab_Click" Text="GoTo Filters tab" Width="120px" />
                     <br />
                 </asp:Panel>
                 </td>
             <td style="width: 174px">
             </td>
         </tr>
         <tr>
             <td align="center" valign="top" colspan="2">
                 <asp:Panel ID="pnlConfirm" runat="server" Visible="False" Width="500px">
                     <table width="500">
                         <tr>
                             <td align="center" style="font-size: small;">
                                 The selection will return&nbsp;
                                 <asp:Label ID="lblCount" runat="server" ForeColor="#00C0C0" Text=" 0 "></asp:Label>
                                 &nbsp;records.<br />
                                 Do you want to continue?</td>
                         </tr>
                         <tr>
                             <td align="center">
                                 <asp:Button ID="btnGetList" runat="server" OnClick="btnGetList_Click" 
                                     Text="Yes, Get the List in EXCEL" Width="170px" />
                                 &nbsp;&nbsp;
                                 <asp:Button ID="btnGetListText" runat="server" OnClick="btnGetListText_Click" 
                                     Text="Yes, Get the List as TEXT" Width="170px" />
                             </td>
                         </tr>
                         <tr>
                             <td align="left" style=" height: 11px">
                             </td>
                         </tr>
                         <tr>
                             <td align="right" colspan="1">
                                 <asp:Button ID="btnCancel" runat="server" BackColor="#FF9999" 
                                     OnClick="btnCancel_Click" Text="Cancel, I'll Revise" Width="135px" />
                             </td>
                         </tr>
                     </table>
                 </asp:Panel>
             </td>
         </tr>
         </table></telerik:RadPageView>
    
</telerik:RadMultiPage>
    <br />
<br />
<div class="clear" id="clear" style="clear: both">&nbsp&nbsp&nbsp&nbsp
   <%-- <asp:HyperLink 
        ID="lnkNewAdmin" runat="server" NavigateUrl="~/AddAdminUser.aspx" 
        Visible="False">Add New Admin</asp:HyperLink>--%>
    <br />&nbsp&nbsp&nbsp</div>
    <br />
        <asp:Panel ID="Panel1" runat="server" >
        <asp:GridView ID="grdListResults" runat="server" Width="1008px">
        </asp:GridView>
    </asp:Panel>
    <br />
    <asp:SqlDataSource ID="sqlGeography" runat="server" ConnectionString="<%$ ConnectionStrings:AirForceDataConnectionString %>"
        SelectCommand="AFDsp_GetAdminGeography" 
    SelectCommandType="StoredProcedure" DataSourceMode="DataReader">
    </asp:SqlDataSource>
    </form>
</asp:Content>



