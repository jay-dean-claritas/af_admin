﻿<%@ Page Title="" Language="C#" MasterPageFile="~/AFD.master" AutoEventWireup="true" CodeFile="ChangePassword.aspx.cs" Inherits="ChangePassword" %>

<%@ Register assembly="Telerik.Web.UI" namespace="Telerik.Web.UI" tagprefix="telerik" %>

<asp:Content ID="Content1" ContentPlaceHolderID="main" Runat="Server">
    <form id="form1" runat="server">
    <p>
        <telerik:RadScriptManager ID="RadScriptManager1" Runat="server">
        </telerik:RadScriptManager>
    </p>
    <p>
        <telerik:RadFormDecorator ID="RadFormDecorator1" Runat="server" Skin="Black" />
        <br />
    </p>
    <p>
        &nbsp;</p>
    <p>
        Provide your old password and your desired new password below. Be sure to 
        confirm the new password in the third box.</p>
    <p>
        &nbsp;</p>
    <p>
        <table style="width: 590px;">
            <tr>
                <td style="width: 136px; height: 39px">
                    UserID:</td>
                <td style="height: 39px; width: 190px">
                    <asp:Label ID="lblUserID" runat="server" Text="Label"></asp:Label>
                </td>
                <td style="height: 39px" align="center">
                    <asp:CheckBox ID="chkShowPWD" runat="server" AutoPostBack="True" 
                        Font-Size="Small" oncheckedchanged="chkShowPWD_CheckedChanged" 
                        Text="Show Passwords" />
                </td>
            </tr>
            <tr>
                <td style="width: 136px; height: 41px">
                    Old Password:</td>
                <td style="height: 41px; width: 190px">
                    <asp:TextBox ID="txtOldPWD" runat="server" TextMode="Password"></asp:TextBox>
                </td>
                <td style="height: 41px">
                    <asp:Label ID="lblWrongOldPassword" runat="server" Font-Size="Small" 
                        ForeColor="#FF5050" 
                        Text="***  Incorrect current password for this account. "></asp:Label>
                </td>
            </tr>
            <tr>
                <td style="width: 136px; height: 47px">
                    New Password:</td>
                <td style="height: 47px; width: 190px">
                    <asp:TextBox ID="txtNewPWD1" runat="server" TextMode="Password"></asp:TextBox>
                </td>
                <td style="height: 47px">
                    <asp:Label ID="lblMisMatchNewPWD" runat="server" Font-Size="Small" 
                        ForeColor="#FF5050" Text="*** New passwords do not match. Please confirm."></asp:Label>
                </td>
            </tr>
            <tr>
                <td style="width: 136px; height: 45px;">
                    Confirm New Password:<br />
                </td>
                <td style="width: 190px; height: 45px;">
                    <asp:TextBox ID="txtNewPWD2" runat="server" TextMode="Password"></asp:TextBox>
                </td>
                <td style="height: 45px">
                    <asp:Label ID="lblSuccess" runat="server" Text="Password successfully changed."></asp:Label>
                </td>
            </tr>
            <tr>
                <td style="width: 136px; height: 34px">
                </td>
                <td style="height: 34px; width: 190px">
                </td>
                <td align="right" style="height: 34px">
                    <asp:Button ID="btnChangePWD" runat="server" onclick="btnChangePWD_Click" 
                        Text="Change PWD" Width="100px" />
                </td>
            </tr>
        </table>
    </p>
    <p>
        &nbsp;</p>
    <p>
        &nbsp;</p>
    </form>
</asp:Content>

