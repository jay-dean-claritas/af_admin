<%@ Page Language="C#" MasterPageFile="~/AFD.master" AutoEventWireup="true" CodeFile="DataStatus.aspx.cs" Inherits="DataStatus" Title="Air Force Data - List Status" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<asp:Content ID="Content1" ContentPlaceHolderID="main" Runat="Server">
    <form id="Form1" runat="server">
    <telerik:RadScriptManager ID="RadScriptManager1" runat="server">
    </telerik:RadScriptManager>
    <br />
    Current list counts:<br />
    <br />
    <table style="width: 1020px">
        <tr>
            <td style="width: 552px">
                <telerik:radgrid id="RadGrid1" runat="server" autogeneratecolumns="False" datasourceid="SqlDataSource1"
                    gridlines="None" skin="Black" width="555px">
<MasterTableView DataSourceID="SqlDataSource1" GridLines="None">
   <GroupByExpressions>
     <telerik:GridGroupByExpression>
       <SelectFields>
          <telerik:GridGroupByField
            FieldName="List"
            HeaderText="List"/>
          <telerik:GridGroupByField
            FieldName="RecordCount"
            HeaderText=" Record Count"
            Aggregate="Sum" />
       </SelectFields>
       <GroupByFields>
          <telerik:GridGroupByField
             FieldName="List"
             SortOrder="Ascending" />
       </GroupByFields>
     </telerik:GridGroupByExpression>
    </GroupByExpressions>
<Columns>
    <telerik:GridBoundColumn DataField="List" DefaultInsertValue="" HeaderText="List"
        SortExpression="List" UniqueName="List">
    </telerik:GridBoundColumn>
<telerik:GridBoundColumn DataField="Source" HeaderText="Source" SortExpression="Source" UniqueName="Source" DefaultInsertValue=""></telerik:GridBoundColumn>
<telerik:GridBoundColumn DataField="RecordCount" HeaderText="Record Count" 
        SortExpression="RecordCount" UniqueName="RecordCount" DataType="System.Int32" 
        DefaultInsertValue="" DataFormatString="{0:n0}">
    <ItemStyle HorizontalAlign="Right" />
    </telerik:GridBoundColumn>
    <telerik:GridBoundColumn DataField="EmailCount" DataFormatString="{0:n0}" 
        DefaultInsertValue="" HeaderText="Email Count" SortExpression="EmailCount" 
        UniqueName="EmailCount">
        <ItemStyle HorizontalAlign="Right" />
    </telerik:GridBoundColumn>
</Columns>
</MasterTableView>
                    <ClientSettings AllowDragToGroup="True">
                    </ClientSettings>
</telerik:radgrid>
            </td>
            <td valign="top" style="padding: 10px; font-size: small">
                Data refreshed July 2016<br />
					 <br />
        </tr>
    </table>
    <br />
    <br />
    <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:AirForceDataConnectionString %>"
        SelectCommand="Select List, a.Source, sum(RecordCount) as 'RecordCount', Sum(EmailCount) as 'EmailCount', max(DisplayOrder) from dbo.SquadronTotalCounts a
left outer join [dbo].[Sources] b
on a.Source = b.Source
group by List, a.Source
order by List,max(DisplayOrder)
">
    </asp:SqlDataSource>
    <br />
    <br />
    </form>
</asp:Content>

