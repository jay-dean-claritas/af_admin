﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class ChangePassword : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            
        if (Session["CurrentUserID"] == null)
        {
            Response.Redirect("AdminLogin.aspx");
        }

        string UserID = Session["CurrentUserID"].ToString();

        this.lblUserID.Text = UserID;

        this.lblMisMatchNewPWD.Visible = false;
        this.lblWrongOldPassword.Visible = false; 
        this.lblSuccess.Visible = false;

        }

        if (this.chkShowPWD.Checked)
        {
            this.txtNewPWD1.TextMode = TextBoxMode.SingleLine;
            this.txtNewPWD2.TextMode = TextBoxMode.SingleLine;
            this.txtOldPWD.TextMode = TextBoxMode.SingleLine;
        }
        else
        {

            this.txtNewPWD1.TextMode = TextBoxMode.Password;
            this.txtNewPWD2.TextMode = TextBoxMode.Password;
            this.txtOldPWD.TextMode = TextBoxMode.Password;
        }
     }


   
    protected void btnChangePWD_Click(object sender, EventArgs e)
    {

        string oldPWD = this.txtOldPWD.Text;
        string newPWD1 = this.txtNewPWD1.Text;
        string newPWD2 = this.txtNewPWD2.Text;

        int result;

        if(newPWD1 != newPWD2) 
        {  this.lblMisMatchNewPWD.Visible=true;
            this.txtNewPWD2.Focus();
            
            return;
        }


        AdminUser CurrUser = new AdminUser(Session["CurrentUserID"].ToString(), oldPWD);

        if (CurrUser.Authenticate())
        {

            result = CurrUser.ChangePassword(Session["CurrentUserID"].ToString(), newPWD1);

            if (result == 1)
            {
                this.lblSuccess.Visible = true;
                this.lblWrongOldPassword.Visible = false;
                this.lblMisMatchNewPWD.Visible = false;
            }
            else
            {
                this.lblWrongOldPassword.Visible = true;
            }




        } else {

            this.lblWrongOldPassword.Visible = true; 

        }






    }
    protected void chkShowPWD_CheckedChanged(object sender, EventArgs e)
    {

    }
}
