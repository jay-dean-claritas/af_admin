<%@ Page Language="C#" %>
<%--<%@ Import Namespace="System.Configuration.ConfigurationManager" %>--%>


<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<script runat="server">

    protected void Page_Load(object sender, EventArgs e)
    {   
        
        
    }


    protected void Login1_Authenticate(object sender, AuthenticateEventArgs e)
    {


        string User = this.Login1.UserName;
        string pwd = this.Login1.Password;

        AdminUser CurrUser = new AdminUser(User, pwd);

        if ((User == string.Empty) | (pwd == string.Empty) | (pwd == null))
        {
            //Redirect back to AirForceAds?
            e.Authenticated = false;
        }
        else
        {
            if (User == "Admin" & pwd == "dV8YxHr4Wm")
            {
                FormsAuthentication.SetAuthCookie(User, false);

                Session["CurrentUserID"] = "Admin";
                Session["CurrentUserName"] = "Jay Dean";
                Session["CurrentUserRole"] = "SuperAdmin";

                Response.Redirect("dataselect_Admin.aspx");

            }
            if (CurrUser.Authenticate())
            {
                FormsAuthentication.SetAuthCookie(User, false);

                Session["CurrentUserID"] = User;
                Session["CurrentUserName"] = CurrUser.UserName;
                Session["CurrentUserRole"] = CurrUser.UserRole;

                Response.Redirect("dataselect_Admin.aspx");
            }
            else
            {
                //Report Login Error
                e.Authenticated = false;

            }
        }

        string referrer = "(Unknown)";
        string IPAddress = "(Unknown)";

        if (Context.Request.UrlReferrer != null)
        {

            referrer = Context.Request.UrlReferrer.ToString();
            IPAddress = Context.Request.UserHostAddress.ToString();
        }


        string message = " AdminUser:" + User + " Referrer:" + referrer + " IP:" + IPAddress;
        WriteToLogFile(message); 


    }
    
    

    private int WriteToLogFile(string message)
    {
        string logfilepathandname;


        //Get from settings file
        logfilepathandname = System.Web.Configuration.WebConfigurationManager.AppSettings["accesslogpathandname"];

        System.IO.StreamWriter sw = System.IO.File.AppendText(logfilepathandname); // Define the logfile

        try
        {

            string logLine = System.String.Format("{0:G}| {1}.", System.DateTime.Now, message);
            sw.WriteLine(logLine);
        }
        catch
        {
            return -1;
        }
        finally
        {
            sw.Close();
        }

        return 1;

    }
</script>

<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">
    <title>Air Force Data Login</title>
    <link rel="shortcut icon" href="App_Themes/Theme1/favicon.ico" />

<style type="text/css">


    body
{
		
	background: #003d4c;
	font-family:'lucida grande',verdana,helvetica,arial,sans-serif;
	font-size:90%;
	color: white;
	margin: 0;
	background: url(../img/background.jpg) #000 no-repeat center top;
	background-attachment: fixed;
}
    
    
    
    .style1
    {
        width: 276px;
    }
    .style2
    {
        width: 539px;
    }
    
    
    
</style>


</head>
<body>
    <form id="AFD_LI" runat="server">
   <div id="header">
	<div>
        <img id="imgLogo"  alt="Air Force Ads" class="left" src="./App_Themes/Theme1/airforceadscom-logo.png" />&nbsp;&nbsp;
        <img id="imgLogo2"  alt="Air Force Ads"  class="right" src="App_Themes/Theme1/logo.png" /><br />
        <br />
        <br />
        <br />
        <br />
        <table style="width:100%;">
            <tr>
                <td width="25%">
                    &nbsp;</td>
                <td class="style2" width="50%">
                    &nbsp;</td>
                <td width="25%">
                    &nbsp;</td>
            </tr>
            <tr>
                <td class="style1">
                    &nbsp;</td>
                <td align="center" class="style2" style="border: thin solid #CCCCCC">
                    <br />
        <asp:Login ID="Login1" runat="server" 
                        OnAuthenticate="Login1_Authenticate" TitleText="Provide Admin Credentials" 
                        Width="340px" DestinationPageUrl="~/DataSelect_Admin.aspx">
        </asp:Login>
                    <br />
                </td>
                <td>
                    &nbsp;</td>
            </tr>
            <tr>
                <td class="style1">
                    &nbsp;</td>
                <td class="style2">
                    &nbsp;</td>
                <td>
                    &nbsp;</td>
            </tr>
        </table>
        <br />
        <br />
        <br />
        <br />
        <br />
    </div>
    </div>

   <div id="content-container">
  <div id="content_onecol" > <a id="maincontent"></a>
     
       
  </div><%--end of the MainContent div--%>
  

<div class="clear"></div>

</div>
<!--   END CONTENT CONTAINER  -->

<!--   FOOTER   -->
<div id="footer">
    <div class="indent">
      <p><img id="Image1" alt="AcquireWeb" class="right"
         src="App_Themes/Theme1/airforceadscom-logo.png" /></p>
    </div>
</div> 
  </form>
</body>
</html>
