using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

/// <summary>
/// Summary description for Squadron
/// </summary>
public class Squadron
{

#region Variables;


    private string _SquadronID;
    private string _Code;
    

#endregion

	public Squadron()
	{

        _SquadronID = string.Empty;
        _Code = string.Empty;

	}

    public Squadron(string sdrn, string code)
    {

        _SquadronID = sdrn;
        _Code = code;

    }


    public bool Validate()
    {

        bool response;


       switch (_SquadronID.ToUpper().Substring(0,2))
        {
            case "H3":
               //Test for this squadron
                if (_Code == "ouw7nfou82ljs")
                {
                    response = true;
                }
                else
                {
                    response = false;
                }
                break;

            case "H2":
                //Test for this squadron
                if (_Code == "ouw7nfou82ljs")
                {
                    response = true;
                }
                else
                {
                    response = false;
                }
                break;

            case "H1":
                //Test for this squadron
                if (_Code == "ouw7nfou82ljs")
                {
                    response = true;
                }
                else
                {
                    response = false;
                }
                break;

            default:
                //Test for this squadron

                response = false;
                                
                break;


                

        }
        return response;
  
    }



}
