﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Security.Cryptography;
using System.Text;


/// <summary>
/// Summary description for AdminUser
/// </summary>
public class AdminUser
{

    #region Variables;

    private int _UserKey;
    private string _UserID;
    private string _PWD;
    private string _UserName;
    private string _UserRole;
    private string _UserEmail;
    private bool _Authenticated = false;



    #endregion

	public AdminUser(string User, string password)
	{
        _UserID = User;

        //if (User != "Admin")
        //{
            _PWD = HashString(password);
        //}
        //else
        //{
        //    _PWD = password;
        //}

        _UserName = string.Empty;
        _UserRole = string.Empty;
        _UserEmail = string.Empty;

	}



    private string HashString(string cleartext)
    {
        // Create an instance of the SHA1 provider
        SHA1 sha = new SHA1CryptoServiceProvider();

        // Compute the hash 
        byte[] hashedData = sha.ComputeHash(Encoding.Unicode.GetBytes(cleartext));

        StringBuilder stringBuilder = new StringBuilder();

        foreach (byte b in hashedData)
        {
            // Convert each byte to Hex
            stringBuilder.Append(String.Format("{0,2:X2}", b));
        }

        // Return the hashed value
        return stringBuilder.ToString();


    }

    public bool Authenticate()
    {
        bool result = false;

        //Connect the Connection Object
        SqlConnection dbConn = ConnectToDB();

        SqlCommand cmd = new SqlCommand("AWsp_GetAdminUserInfo", dbConn);

        cmd.CommandType = CommandType.StoredProcedure;

        SqlParameter Param1 = new SqlParameter("@UserID", SqlDbType.VarChar, 30);
        Param1.Value = _UserID;
        cmd.Parameters.Add(Param1);

        SqlParameter Param2 = new SqlParameter("@PWD", SqlDbType.VarChar, 100);
        cmd.Parameters.Add(Param2);
        Param2.Value = _PWD;

        SqlDataReader rdr = cmd.ExecuteReader(CommandBehavior.CloseConnection);

        rdr.Read();

        if (rdr.HasRows) 
        {
            result = true;
            _Authenticated = true;
                if (!rdr.IsDBNull(0)) { _UserKey = rdr.GetInt32(0); }
                if (!rdr.IsDBNull(1)) { _UserName = rdr.GetString(1); }
                if (!rdr.IsDBNull(2)) { _UserEmail = rdr.GetString(2); }
                if (!rdr.IsDBNull(3)) { _UserRole = rdr.GetString(3); }   
        }


        dbConn.Close();
        dbConn.Dispose();


        return result;
    }



    private System.Data.SqlClient.SqlConnection ConnectToDB()
    {
        System.Data.SqlClient.SqlConnection dbConn = new SqlConnection();
        string strConnection = System.Configuration.ConfigurationManager.ConnectionStrings["AirForceDataConnectionString"].ConnectionString;
        dbConn.ConnectionString = strConnection;

        dbConn.Open();

        return dbConn;
    }





    public int AddNewAdmin(String UserName, String UserEmail, string Role)
    {
        int result;

        //Connect the Connection Object
        SqlConnection dbConn = ConnectToDB();

        SqlCommand cmd = new SqlCommand("AWsp_InsertAdminUser", dbConn);

        cmd.CommandType = CommandType.StoredProcedure;

        SqlParameter Param1 = new SqlParameter("@UserID", SqlDbType.VarChar, 30);
        Param1.Value = _UserID;
        cmd.Parameters.Add(Param1);

        SqlParameter Param2 = new SqlParameter("@PWD", SqlDbType.VarChar, 100);
        cmd.Parameters.Add(Param2);
        Param2.Value = _PWD;

        SqlParameter Param3 = new SqlParameter("@UserName", SqlDbType.VarChar, 30);
        cmd.Parameters.Add(Param3);
        Param3.Value = UserName;

        SqlParameter Param4 = new SqlParameter("@UserEmail", SqlDbType.VarChar, 50);
        cmd.Parameters.Add(Param4);
        Param4.Value = UserEmail;

        SqlParameter Param5 = new SqlParameter("@Role", SqlDbType.VarChar, 30);
        cmd.Parameters.Add(Param5);
        Param5.Value = Role;

        result = cmd.ExecuteNonQuery();

        dbConn.Close();
        dbConn.Dispose();


        return result;
    }


    
    public int ChangePassword(String UserID, String NewPassword)
    {
        int result;
        string NewPWD;



        //if (UserID != "Admin")
        //{
            NewPWD = HashString(NewPassword);
        //}
        //else
        //{
        //    NewPWD = NewPassword;
        //}

       

        //Connect the Connection Object
        SqlConnection dbConn = ConnectToDB();

        SqlCommand cmd = new SqlCommand("AWsp_UpdateAdminPWD", dbConn);

        cmd.CommandType = CommandType.StoredProcedure;

        SqlParameter Param1 = new SqlParameter("@UserID", SqlDbType.VarChar, 30);
        Param1.Value = _UserID;
        cmd.Parameters.Add(Param1);

        SqlParameter Param2 = new SqlParameter("@PWD", SqlDbType.VarChar, 100);
        cmd.Parameters.Add(Param2);
        Param2.Value = _PWD;

        SqlParameter Param3 = new SqlParameter("@NewPWD", SqlDbType.VarChar, 100);
        cmd.Parameters.Add(Param3);
        Param3.Value = NewPWD;
        
        result = cmd.ExecuteNonQuery();

        dbConn.Close();
        dbConn.Dispose();


        return result;
    }



    public int ResetPWD(string UserID, string newpassword)
    {
        int result = 0;

        string NewPWD = HashString(newpassword);


        //Connect the Connection Object
        SqlConnection dbConn = ConnectToDB();

        SqlCommand cmd = new SqlCommand("AWsp_ResetAdminPwd", dbConn);

        cmd.CommandType = CommandType.StoredProcedure;

        SqlParameter Param1 = new SqlParameter("@UserID", SqlDbType.VarChar, 30);
        Param1.Value = UserID;
        cmd.Parameters.Add(Param1);

        SqlParameter Param2 = new SqlParameter("@NewPWD", SqlDbType.VarChar, 100);
        cmd.Parameters.Add(Param2);
        Param2.Value = NewPWD;

        result = cmd.ExecuteNonQuery();

        dbConn.Close();
        dbConn.Dispose();


        return result;

    }



    public string UserName
    {
        get
        {
            return _UserName;
        }

    }

    public string UserRole
    {
        get
        {
            return _UserRole;
        }

    }


}
