using System;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Web;
using System.Web.Security;
//using System.Web.UI;
//using System.Web.UI.WebControls;
//using System.Web.UI.WebControls.WebParts;
//using System.Web.UI.HtmlControls;

/// <summary>
/// Summary description for ListSelect
/// </summary>
/// 
[Serializable]
public class ListSelect
{

#region Variables;

    private string _geolist;
    private string _source;
    private bool _complete;
    private string _listKey;
    private int _recordCount;
    private int _zipCount;
    private string _squadron;
    private string _AdminUser;
    private string _GenderCode = "b";
    private bool _HomeAddr = false;
    private bool _WorkAddr = false;
    private bool _EmailAddr = false;
    private string _SpecList = "";




    

#endregion

#region Properties

    public  int RecordCount
    {
        get { return _recordCount; }
        //set { _recordCount = value; }
    }

    public  int ZipCount
    {
        get { return _zipCount; }
       // set { _zipCount = value; }
    }

    public string Squadron
    {
        get { return _squadron; }
        set { _squadron = value; }
    }

    public string AdminUser
    {
        get { return _AdminUser; }
        set { _AdminUser = value; }
    }


    public  bool Complete
    {
        get { return _complete; }
    }


#endregion



#region PublicMethods;


    public ListSelect(string User, string Source)
	{
		//
        _complete = false;
        _recordCount = 0;
        _zipCount = 0;
        _AdminUser = User;
        _source = Source;

    }

 


    public int GetCount(string GeoList, string GenderCode, string SpecList, bool WorkAddress, bool HomeAddress, bool EmailAddress )
    {

        _geolist = GeoList;
        _GenderCode = GenderCode;
        _HomeAddr = HomeAddress;
        _WorkAddr = WorkAddress;
        _EmailAddr = EmailAddress;
        _SpecList = SpecList;

        //string Proc = ProcedureName_Count(_source);

        //Connect the Connection Object
        SqlConnection dbConn = ConnectToDB();
		SqlCommand cmd;

		if (_source == "Vocational/Trade Schools" || _source == "Linguistics" || _source == "Drop-Outs" || _source == "Community College" || _source == "Aviation" || _source == "SPECIAL OPS" || _source == "STEM" || _source == "PRIOR SERVICE")
		{

			cmd = new SqlCommand("AFDsp_CountEnlisted_StateList", dbConn);

			cmd.CommandType = CommandType.StoredProcedure;

			SqlParameter Param1 = new SqlParameter("@StateList", SqlDbType.VarChar, 2000);
			Param1.Value = _geolist;
			cmd.Parameters.Add(Param1);

			SqlParameter Param2 = new SqlParameter("@Source", SqlDbType.VarChar, 80);
			Param2.Value = _source;
			cmd.Parameters.Add(Param2);
		}
		else if (_source == "Reengagement")
		{

			cmd = new SqlCommand("AFDsp_CountReengagementByState", dbConn);

			cmd.CommandType = CommandType.StoredProcedure;

			SqlParameter Param1 = new SqlParameter("@StateList", SqlDbType.VarChar, 2000);
			Param1.Value = _geolist;
			cmd.Parameters.Add(Param1);

		}
		else
		{

			//SqlCommand cmd = new SqlCommand(Proc, dbConn);
			cmd = new SqlCommand("AFDsp_CountAFD_StateList_2", dbConn);

			cmd.CommandType = CommandType.StoredProcedure;

			SqlParameter Param1 = new SqlParameter("@StateList", SqlDbType.VarChar, 2000);
			Param1.Value = _geolist;
			cmd.Parameters.Add(Param1);

			SqlParameter Param2 = new SqlParameter("@Source", SqlDbType.VarChar, 80);
			Param2.Value = _source;
			cmd.Parameters.Add(Param2);

			SqlParameter Param3 = new SqlParameter("@HomeAddrPresent", SqlDbType.Bit);
			Param3.Value = HomeAddress;
			cmd.Parameters.Add(Param3);

			SqlParameter Param4 = new SqlParameter("@WorkAddrPresent", SqlDbType.Bit);
			Param4.Value = WorkAddress;
			cmd.Parameters.Add(Param4);

			SqlParameter Param5 = new SqlParameter("@EmailAddrPresent", SqlDbType.Bit);
			Param5.Value = EmailAddress;
			cmd.Parameters.Add(Param5);

			SqlParameter Param6 = new SqlParameter("@Gender", SqlDbType.Char, 1);
			Param6.Value = GenderCode;
			cmd.Parameters.Add(Param6);

			SqlParameter Param7 = new SqlParameter("@SpecList", SqlDbType.VarChar, 2000);
			Param7.Value = SpecList;
			cmd.Parameters.Add(Param7);

		}

        SqlDataReader rdr = cmd.ExecuteReader(CommandBehavior.CloseConnection);

        rdr.Read();

        if (!rdr.HasRows) { throw new System.Exception("Query failed"); }

        //_recordCount = 0;

        //string querytext = rdr.GetString(0);

        if (!rdr.IsDBNull(0)) { _recordCount = rdr.GetInt32(0); }
        //if (!rdr.IsDBNull(1)) { _zipCount = rdr.GetInt32(1); }

        dbConn.Dispose();

        return _recordCount;

    }

    public SqlDataReader GetList(string UserID)
    {

        SqlDataReader result = GetListData(_geolist, _GenderCode, _SpecList, _WorkAddr, _HomeAddr, _EmailAddr) ;

        return result;

        //Log the query

    }


    public SqlDataReader GetList(string GeoList, string GenderCode, string SpecList, bool WorkAddress, bool HomeAddress, bool EmailAddress)
    {

        _geolist = GeoList;
        _GenderCode = GenderCode;
        _HomeAddr = HomeAddress;
        _WorkAddr = WorkAddress;
        _EmailAddr = EmailAddress;
        _SpecList = SpecList;



        SqlDataReader result = GetListData(_geolist, _GenderCode, _SpecList, _WorkAddr, _HomeAddr, _EmailAddr);

        return result;

        //Log the query


    }




    private SqlDataReader GetListData(string GeoList, string GenderCode, string SpecList, bool WorkAddress, bool HomeAddress, bool EmailAddress)
    {

        _geolist = GeoList;

        
        //string Proc = ProcedureName_Get(_source);

        //Connect the Connection Object
        SqlConnection dbConn = ConnectToDB();

        //Log the request
        LogRequest(dbConn);

		SqlCommand cmd;

		if (_source == "Vocational/Trade Schools" || _source == "Linguistics" || _source == "Drop-Outs" || _source == "Community College" || _source == "Aviation" || _source == "SPECIAL OPS" || _source == "STEM" || _source == "PRIOR SERVICE")
		{

			cmd = new SqlCommand("AFDsp_GetEnlisted_StateList", dbConn);

			cmd.CommandType = CommandType.StoredProcedure;

			SqlParameter Param1 = new SqlParameter("@StateList", SqlDbType.VarChar, 2000);
			Param1.Value = _geolist;
			cmd.Parameters.Add(Param1);

			SqlParameter Param2 = new SqlParameter("@Source", SqlDbType.VarChar, 80);
			Param2.Value = _source;
			cmd.Parameters.Add(Param2);



		}
		else if (_source == "Reengagement")
		{

			cmd = new SqlCommand("AFDsp_GetReengagementByState", dbConn);

			cmd.CommandType = CommandType.StoredProcedure;

			SqlParameter Param1 = new SqlParameter("@StateList", SqlDbType.VarChar, 2000);
			Param1.Value = _geolist;
			cmd.Parameters.Add(Param1);

		}
		else
		{


			if (_source.Contains("MCAT") || _source.Contains("DAT"))
			{
				cmd = new SqlCommand("AFDsp_GetAFD_StateList_MCAT", dbConn);
			}
			else
			{
				cmd = new SqlCommand("AFDsp_GetAFD_StateList_2", dbConn);
			}

			//SqlCommand cmd = new SqlCommand(Proc, dbConn);
			

			cmd.CommandType = CommandType.StoredProcedure;

			SqlParameter Param1 = new SqlParameter("@StateList", SqlDbType.VarChar, 2000);
			Param1.Value = _geolist;
			cmd.Parameters.Add(Param1);

			SqlParameter Param2 = new SqlParameter("@Source", SqlDbType.VarChar, 80);
			Param2.Value = _source;
			cmd.Parameters.Add(Param2);

			SqlParameter Param3 = new SqlParameter("@HomeAddrPresent", SqlDbType.Bit);
			Param3.Value = HomeAddress;
			cmd.Parameters.Add(Param3);

			SqlParameter Param4 = new SqlParameter("@WorkAddrPresent", SqlDbType.Bit);
			Param4.Value = WorkAddress;
			cmd.Parameters.Add(Param4);

			SqlParameter Param5 = new SqlParameter("@EmailAddrPresent", SqlDbType.Bit);
			Param5.Value = EmailAddress;
			cmd.Parameters.Add(Param5);

			SqlParameter Param6 = new SqlParameter("@Gender", SqlDbType.Char, 1);
			Param6.Value = GenderCode;
			cmd.Parameters.Add(Param6);

			SqlParameter Param7 = new SqlParameter("@SpecList", SqlDbType.VarChar, 2000);
			Param7.Value = SpecList;
			cmd.Parameters.Add(Param7);

		}

        SqlDataReader rdr = cmd.ExecuteReader(CommandBehavior.CloseConnection);

        rdr.Read();
        
        if (!rdr.HasRows) { throw new System.Exception("Query failed"); }

        //dbConn.Close();

        return rdr;

    }
        

#endregion


#region PrivateMethods;

        private System.Data.SqlClient.SqlConnection ConnectToDB()
        {
            System.Data.SqlClient.SqlConnection dbConn = new SqlConnection();
            string strConnection = System.Configuration.ConfigurationManager.ConnectionStrings["AirForceDataConnectionString"].ConnectionString;
            dbConn.ConnectionString = strConnection;

            dbConn.Open();

            return dbConn;
        }

     private string ProcedureName_Get(string source)
        {
        
        string Proc = string.Empty;

        if (source == "Reengagement") { Proc = "AFDsp_GetReengagementByBillet"; }
        else { Proc = "AFDsp_GetData_StateList"; }

        //if (source.Contains("Dentist")) {Proc = "AFDsp_GetDentists_StateList_D";}
        //else if (source.Contains("Nurse")) { Proc = "AFDsp_GetNurses_StateList_D"; }
        //else if (source.Contains("PhysiciansAssistant")) { Proc = "AFDsp_GetPhysiciansAssistants_StateList_D"; }
        //else if (source.Contains("Physician")) { Proc = "AFDsp_GetPhysicians_StateList_D"; }
        //else if (source.Contains("Psychologist")) { Proc = "AFDsp_GetPsychologists_StateList_D"; }
        //else if (source.Contains("MCAT")) { Proc = "AFDsp_GetMCAT_StateList_D"; }
        //else if (source.Contains("DAT")) { Proc = "AFDsp_GetDAT_StateList_D"; }
        //else if (source.Contains("Electrical")) { Proc = "AFDsp_GetElectEngineers_StateList_D"; }

        //else if (source.Contains("SpecialSocial")) { Proc = "AFDsp_GetSpecialSocialWorkers_StateList_D"; }
        //else if (source.Contains("SP_Social")) { Proc = "AFDsp_GetSpecialSocialWorkers_StateList_D"; }

        //else if (source.Contains("Therapists")) { Proc = "AFDsp_GetPhysicalTherapists_StateList_D"; }
        //else { Proc = "AFDsp_GetClinicalSocialWorkers_StateList_D"; }

        return Proc;

    }


    private string ProcedureName_Count(string source)
    {

        string Proc = string.Empty;

        if (source == "Reengagement") { Proc = "AFDsp_GetReengagementByBillet"; }
        else { Proc = "[AFDsp_Count_StateList]"; }

        //if (source.Contains("Dentist")) { Proc = "AFDsp_CountDentists_StateList"; }
        //else if (source.Contains("Nurse")) { Proc = "AFDsp_CountNurses_StateList"; }
        //else if (source.Contains("PhysiciansAssistant")) { Proc = "AFDsp_CountPhysiciansAssistants_StateList"; }
        //else if (source.Contains("Physician")) { Proc = "AFDsp_CountPhysicians_StateList"; }
        //else if (source.Contains("Psychologist")) { Proc = "AFDsp_CountPsychologists_StateList"; }
        //else if (source.Contains("MCAT")) { Proc = "AFDsp_CountMCAT_StateList"; }
        //else if (source.Contains("DAT")) { Proc = "AFDsp_CountDAT_StateList"; }
        //else if (source.Contains("Electrical")) { Proc = "AFDsp_CountElecEngineers_RegionKeyList"; }
        //else if (source.Contains("SpecialSocial")) { Proc = "AFDsp_CountSpecialSocialWorkers_StateList"; }
        //else if (source.Contains("SP_Social")) { Proc = "AFDsp_CountSpecialSocialWorkers_StateList"; }
        //else if (source.Contains("Therapists")) { Proc = "AFDsp_CountPhysicalTherapists_StateList"; }

        //else { Proc = "AFDsp_CountClinicalSocialWorkers_StateList"; }

        return Proc;
    }



    private void LogRequest(SqlConnection dbConn)
    {
        
        SqlCommand cmd = new SqlCommand("AFDsp_LogDataRequest", dbConn);

        cmd.CommandType = CommandType.StoredProcedure;


        SqlParameter Param1 = new SqlParameter("@RegionKeyList", SqlDbType.Text);
        Param1.Value = _geolist;
        cmd.Parameters.Add(Param1);

        cmd.Parameters.Add(new SqlParameter("@Source", _source));

        cmd.Parameters.Add(new SqlParameter("@Squadron", _AdminUser));

        cmd.Parameters.Add(new SqlParameter("@ZipCount", '0'));

        cmd.Parameters.Add(new SqlParameter("@ResultCount", _recordCount));

        int returnval = cmd.ExecuteNonQuery();


        //dbConn.Dispose();


    }



#endregion



}
