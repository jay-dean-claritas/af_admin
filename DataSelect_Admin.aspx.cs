using System;
using System.Data;

using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Text;
using System.IO;
using Telerik.Web.UI;

public partial class DataSelect : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

        if (Session["CurrentUserID"] == null)
        {
            Response.Redirect("AdminLogin.aspx");
        }

        string UserID = Session["CurrentUserID"].ToString();
        string role = Session["CurrentUserRole"].ToString();

        if (role == "SuperAdmin")
        {
            //this.lnkNewAdmin.Visible = true;
        }


        if (!Page.IsPostBack)
        {
            this.tvGeography.DataBind();
            if (this.tvGeography.Nodes.Count > 0)
            {
                this.tvGeography.Nodes[0].Expanded = true;
            }

            this.RadTabStrip1.SelectedIndex = 0;

        }

    }


    protected void btnGetCount_Click(object sender, EventArgs e)
    {
        bool Error = false;
        if (this.tvLists.SelectedNodes.Count == 0)
        {
            //Report the error
            this.pnlNoList.Visible = true;
            Error = true;
            this.RadTabStrip1.SelectedIndex = 3;
        }
        else this.pnlNoList.Visible = false;

        if (this.tvGeography.CheckedNodes.Count == 0)
        {
            //Report the error
            this.pnlNoGeo.Visible = true;
            Error = true;
        }
        else this.pnlNoGeo.Visible = false;

        if (Error)
        {
            this.pnlConfirm.Visible = false;

            return;
        }
        this.RadTabStrip1.SelectedIndex = 3;

        string geoList = GetListOfLeafNodes_Geo();
        string source = this.tvLists.SelectedValue.ToString();

        string Gendercode = GetGenderCode();

        bool WorkAddr = this.chkWorkAddress.Checked;
        bool HomeAddr = this.chkHomeAddress.Checked;
        bool EmailAddr = this.chkEmailAddress.Checked;

        string SpecList = GetSpecList();


        Session.Add("currentList", source);

        string UserID = Session["CurrentUserID"].ToString();

        ListSelect Selection = new ListSelect(UserID, source);

        int SelectCount = Selection.GetCount(geoList, Gendercode, SpecList, WorkAddr, HomeAddr, EmailAddr);

        if (SelectCount == 0)
        {
            this.pnlNoCount.Visible = true;
            //this.tvGeography.CollapseAllNodes();
            this.pnlConfirm.Visible = false;
            this.btnGetCount.Enabled = true;
            this.tvGeography.Enabled = true;
            this.tvLists.Enabled = true;
            return;
        }
        else
        {
            this.pnlNoCount.Visible = false;

        }

        if (SelectCount > 50000)
        {
            this.btnGetList.Enabled = false;
        }
        else
        {
            this.btnGetList.Enabled = true;
        }



        this.lblCount.Text = SelectCount.ToString();        
        this.tvGeography.Enabled = false;
        this.tvLists.Enabled = false;

        //this.tvGeography.CollapseAllNodes();
        this.pnlConfirm.Visible = true;
        this.btnGetCount.Enabled = false;

        //Add the List Select object to the Session
        Session.Add("CurrentSelect", Selection);

        
    }

    protected string GetGenderCode()
    {
        string Code = "b";

        if (this.rbGender_F.Checked)
        {
            Code = "f";
        } else if (this.rbGender_m.Checked)
        {
            Code = "m";
        } 

        return Code;

    }

    protected string GetListOfLeafNodes_Geo()
    {
        //int maxlvl = this.tvGeography.;
        //List<string> zips = new List<string>();
        string result = "";

        string connect = "";

       //Get the of list of selected nodes
            foreach (Telerik.Web.UI.RadTreeNode Node in this.tvGeography.CheckedNodes)
            {

                if (Node.Value != "All")
                {
                    result = result + connect + "'" + Node.Value + "'";
                    connect = ", ";
                }
                else
                {

                    result = "All";
                    break;
                }

            }


        Session.Add("currentGeoList", result);

        return result;

    }

    protected string GetSpecList()
    {
        string result = "";
        string connect = "";

        if (this.lstSpecialty.Enabled)
        {
            foreach (Telerik.Web.UI.RadListBoxItem Spec in this.lstSpecialty.CheckedItems)
            {
                result = result + connect + "'" + Spec.Value + "'";
                connect = ", ";

            }
        }

        return result;

    }



    protected void RadTabStrip1_TabClick(object sender, RadTabStripEventArgs e)
    {
        Telerik.Web.UI.RadTab TabClicked = e.Tab;

        if (TabClicked.PageViewID == "pvFilters")
        {

            SetOtherFiltersControls();
                    
        } else if (TabClicked.PageViewID == "pvCounts") {

            this.pnlConfirm.Visible = false;
            this.pnlNoCount.Visible = false;
            this.pnlNoGeo.Visible = false;
            this.pnlNoList.Visible = false;

        }

    }


    protected void SetOtherFiltersControls()
    {
        if (this.tvLists.SelectedNodes.Count > 0)
        {

            switch (this.tvLists.SelectedNode.Value)
            {
                case "All_Nurses":
                case "Nurse_AMI":    
                case "Scholarship_Nurses":
                    this.lblSpecialties.Text = "Specialties for: NURSES";
                    FillNursesSpecialties();
                    this.lstSpecialty.Enabled = true;
                    break;
                case "All_Physicians":
                case "Physicians_AMI":
                case "Physicians SDC":
                case "Physicians_Scholarships":
                    this.lblSpecialties.Text = "Specialties for: Physicians";
                    FillPhysicianSpecialties();
                    this.lstSpecialty.Enabled = true;
                    break;
                case "All_Dentists":
                case "Dentists_AMI":
                case "Dentist_Scholarships":

                    this.lblSpecialties.Text = "Specialties for: Dentists";
                    FillDentistSpecialties();
                    this.lstSpecialty.Enabled = true;
                    break;
				case "Specialty":
				case "STEM":
				case "PriorService":
					this.lblSpecialties.Text = "No specialties for this list";
                    this.lstSpecialty.Items.Clear();
                    this.lstSpecialty.Enabled = false;
					//this.chkEmailAddress.Enabled = false;
					
                    break;

                default:
                    this.lblSpecialties.Text = "No specialties for this list";
                    this.lstSpecialty.Items.Clear();
                    this.lstSpecialty.Enabled = false;
                    break;
            }

            switch (this.tvLists.SelectedNode.Value)
            {
                case "All_Nurses":
                case "Nurse AMI":
                case "All_Physicians":
                case "Physicians AMI":
                case "Physicians SDC":
                case "All_Dentists":
                case "Dentists AMI":
                case "All_ClinicalSocialWorkers":
                case "Electrical Engineering Students_SMG":
                case "Clinical Social Worker SDC":
                    this.rbGender_F.Enabled = true;
                    this.rbGender_m.Enabled = true;
                    
                    break;
                default:
                    this.rbGender_F.Enabled = false;
                    this.rbGender_m.Enabled = false;
                    this.rbGender_b.Checked = true;
                    break;
            }

            if (this.tvLists.SelectedNode.Value.Contains("MCAT"))
            {
                this.rbGender_F.Enabled = true;
                this.rbGender_m.Enabled = true;
            }
        }
        else
        {
            this.lblSpecialties.Text = "No list is selected!";
            this.lstSpecialty.Items.Clear();
            this.lstSpecialty.Enabled = false;
        }

    }


    protected void FillNursesSpecialties()
    {

        this.lstSpecialty.Items.Clear();

        Telerik.Web.UI.RadListBoxItem item1 = new Telerik.Web.UI.RadListBoxItem("Clinical/Medical/Surgical Care Nurse", "N1");
        this.lstSpecialty.Items.Add(item1);

        Telerik.Web.UI.RadListBoxItem item2 = new Telerik.Web.UI.RadListBoxItem("Mental Health Nurse", "N2");
        this.lstSpecialty.Items.Add(item2);

        Telerik.Web.UI.RadListBoxItem item3 = new Telerik.Web.UI.RadListBoxItem("Emergency/Critical Nurse", "N3");
        this.lstSpecialty.Items.Add(item3);

        Telerik.Web.UI.RadListBoxItem item4 = new Telerik.Web.UI.RadListBoxItem("Family Care Nurse", "N4");
        this.lstSpecialty.Items.Add(item4);

    }


    protected void FillPhysicianSpecialties()
    {
        this.lstSpecialty.Items.Clear();

        Telerik.Web.UI.RadListBoxItem item1 = new Telerik.Web.UI.RadListBoxItem("Internal Medicine", "P1");
        this.lstSpecialty.Items.Add(item1);

        Telerik.Web.UI.RadListBoxItem item2 = new Telerik.Web.UI.RadListBoxItem("Family Practice", "P2");
        this.lstSpecialty.Items.Add(item2);


        Telerik.Web.UI.RadListBoxItem item4 = new Telerik.Web.UI.RadListBoxItem("Emergency Physician", "P4");
        this.lstSpecialty.Items.Add(item4);

        Telerik.Web.UI.RadListBoxItem item5 = new Telerik.Web.UI.RadListBoxItem("Psychiatrist", "P5");
        this.lstSpecialty.Items.Add(item5);
 

    }


    protected void FillDentistSpecialties()
    {
        this.lstSpecialty.Items.Clear();

        Telerik.Web.UI.RadListBoxItem item1 = new Telerik.Web.UI.RadListBoxItem("General Dentistry", "D1");
        this.lstSpecialty.Items.Add(item1);

        Telerik.Web.UI.RadListBoxItem item2 = new Telerik.Web.UI.RadListBoxItem("Oral-Maxiofacial Surgery", "D2");
        this.lstSpecialty.Items.Add(item2);

        Telerik.Web.UI.RadListBoxItem item3 = new Telerik.Web.UI.RadListBoxItem("Prosthodontist", "D3");
        this.lstSpecialty.Items.Add(item3);
		
		Telerik.Web.UI.RadListBoxItem item4 = new Telerik.Web.UI.RadListBoxItem("Periodontist", "D4");
        this.lstSpecialty.Items.Add(item4);

    }



    protected void ShowCheckedNodes_Geo()
    {
        //Get the of list of selected nodes
        foreach (Telerik.Web.UI.RadTreeNode Node in this.tvGeography.CheckedNodes)
        {
            Node.ExpandParentNodes();
        }
    }

  

    public override void VerifyRenderingInServerForm(Control control)
    {

    }

   
    protected void btnCancel_Click(object sender, EventArgs e)
    {
        this.tvGeography.Enabled = true;
        this.tvLists.Enabled = true;
        this.pnlConfirm.Visible = false;
        this.btnGetCount.Enabled = true;
        ShowCheckedNodes_Geo();

        this.RadMultiPage1.SelectedIndex = 1;
        this.RadTabStrip1.SelectedIndex = 1;


    }

    protected void btnGetList_Click(object sender, EventArgs e)
    {
        System.Data.SqlClient.SqlDataReader rdr = GetList_Master();

        ExportToExcel(rdr);

    }

    protected void btnGetListText_Click(object sender, EventArgs e)
    {

        System.Data.SqlClient.SqlDataReader rdr = GetList_Master();

        ExportToText(rdr);

    }

    protected System.Data.SqlClient.SqlDataReader GetList_Master()
    {

        string UserID = Session["CurrentUserID"].ToString();
        ListSelect Selection;

        Selection = (ListSelect)Session["CurrentSelect"];
        

        System.Data.SqlClient.SqlDataReader rdr = Selection.GetList(UserID);


        return rdr;

    }



    protected void ExportToExcel(System.Data.SqlClient.SqlDataReader rdr)
    {

        this.grdListResults.DataSource = rdr;
        this.grdListResults.DataBind();


        string filename = Session["CurrentUserID"].ToString() + "_" + Session["currentList"].ToString() + "_" + DateTime.Now.ToString("yyyyMMdd-hhmm") + ".xls";
        filename = CleanFilename(filename);



        Response.ClearContent();
        Response.Clear();
        //Response.Buffer = true;
        Response.Cache.SetCacheability(HttpCacheability.NoCache);
        //Response.AddHeader("content-disposition", attachment)
       
        Response.AddHeader("Content-Disposition", "attachment; filename=" + filename);
        Response.Charset = "";
        this.EnableViewState = false;

        //Response.ContentType = "application/vnd.ms-excel";
        Response.ContentType = "application/vnd.xls";
        //Response.ContentType = "application/excel";

        StringWriter sw = new StringWriter();
        HtmlTextWriter htw = new HtmlTextWriter(sw);

        this.grdListResults.RenderControl(htw);
        Response.Write(sw.ToString());
        Response.Flush();
        Response.End();
    }


    protected string CleanFilename(string rawFileName)
    {

        string response;

        response = rawFileName.Replace(" ", "");
        response = response.Replace("-", "_");


        return response;
    }



    protected void ExportToText(System.Data.SqlClient.SqlDataReader rdr)
    {

        StringBuilder SB = new StringBuilder();
        int n;
        string sValue = String.Empty;
        int f = rdr.FieldCount;


        if (rdr.HasRows)
        {
            sValue = "\t";
            for (n = 0; n < f; n++)
            {
                if (n < f -1)
                {
                    SB.Append(rdr.GetName(n) + sValue);
                }else {
                    SB.Append(rdr.GetName(n));
                }

            }
            //Add the new line character
            SB.Append("\n");
            
            
        }





        if (rdr.HasRows)
        {
            sValue = "\t";
            while (rdr.Read())
            {
                for (n = 0; n < f; n++)
                {
                    if (n < f -1)
                    {
                        SB.Append(rdr.GetValue(n) + sValue);
                    }else {
                        SB.Append(rdr.GetValue(n));
                    }

                }
                //Add the new line character
                SB.Append("\n");
            }
            
        }

        string filename = Session["CurrentUserID"].ToString() + "_" + Session["currentList"].ToString() + "_" + DateTime.Now.ToString("yyyyMMdd-hhmm") + ".txt";

        Response.ClearContent();
        Response.ClearHeaders();
        Response.Clear();
        Response.Buffer = true;
        Response.ContentType = "application/vnd.text";
        //Response.ContentType = "text/csv";
        //Response.ContentType =  "application/vnd.ms-excel";
        Response.AddHeader("Content-Disposition", "attachment; filename=" + filename);
        Response.Charset = "";
        this.EnableViewState = false;
        //StringWriter sw = new StringWriter();
        //HtmlTextWriter htw = new HtmlTextWriter(sw);

        //this.grdListResults.RenderControl(htw);

        Response.Write(SB.ToString());
        Response.Flush();
        Response.End();
    }



protected void  tvGeography_NodeClick(object sender, Telerik.Web.UI.RadTreeNodeEventArgs e)
{

    if (e.Node.Checked)
    {
        e.Node.Checked = false;
    }
    else
    {
        e.Node.Checked = true;
    }
    e.Node.Selected = false;
}


    protected void btnReset_Click(object sender, EventArgs e)
    {
        this.tvGeography.Enabled = true;
        this.tvLists.Enabled = true;
        this.tvLists.ClearSelectedNodes();
        this.btnGetCount.Enabled = true;
        this.pnlConfirm.Visible = false;
		this.pnlNoCount.Visible = false;
        this.tvGeography.ClearCheckedNodes();
        this.tvGeography.CollapseAllNodes();
        if (this.tvGeography.Nodes.Count > 0)
        {
            this.tvGeography.Nodes[0].Expanded = true;
        }

        this.RadTabStrip1.SelectedIndex = 0;
        this.RadMultiPage1.SelectedIndex = 0;


    }
    protected void btnNext1_Click(object sender, EventArgs e)
    {
        this.RadTabStrip1.SelectedIndex = 1;
        this.RadMultiPage1.SelectedIndex = 1;
    }
    protected void btnNext2_Click(object sender, EventArgs e)
    {
        this.RadTabStrip1.SelectedIndex = 2;
        this.RadMultiPage1.SelectedIndex = 2;
        SetOtherFiltersControls();
    }
    protected void btnNext3_Click(object sender, EventArgs e)
    {
        this.RadTabStrip1.SelectedIndex = 3;
        this.RadMultiPage1.SelectedIndex = 3;
       
    }
    protected void btnReturnToListsTab_Click(object sender, EventArgs e)
    {
        this.RadTabStrip1.SelectedIndex = 0;
        this.RadMultiPage1.SelectedIndex = 0;
    }
    protected void btnReturnToGeoTab_Click(object sender, EventArgs e)
    {
        this.RadTabStrip1.SelectedIndex = 1;
        this.RadMultiPage1.SelectedIndex = 1;
    }


    protected void Button1_Click(object sender, EventArgs e)
    {
        this.RadTabStrip1.SelectedIndex = 1;
        this.RadMultiPage1.SelectedIndex = 1;
    }
    protected void btnGotoFiltersTab_Click(object sender, EventArgs e)
    {
        this.RadTabStrip1.SelectedIndex = 2;
        this.RadMultiPage1.SelectedIndex = 2;
    }
    protected void Button2_Click(object sender, EventArgs e)
    {

    }
}

