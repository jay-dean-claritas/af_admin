<%@ Page Language="C#" MasterPageFile="~/AFD.master" AutoEventWireup="true" CodeFile="RequestLog.aspx.cs" Inherits="DataStatus" Title="Data Download Log" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<asp:Content ID="Content1" ContentPlaceHolderID="main" Runat="Server">
    <form id="Form1" runat="server">
    <telerik:RadScriptManager ID="RadScriptManager1" runat="server">
    </telerik:RadScriptManager>
    <br />
    Data Downloads Log for last 12 months:<br />
    <br />
    <table style="width: 1020px">
        <tr>
            <td style="width: 700px; height: 10px;" align="right">
                    <div class = "right" style="height: 32px"  ><telerik:RadToolBar ID="RadToolBar1" 
                            runat="server" OnButtonClick="RadToolBar1_ButtonClick"
        Skin="Black" CssClass="right">
        <Items>
            <telerik:RadToolBarButton runat="server" CommandName="Excel" Text="Excel" ToolTip="Export the report to Excel" Width="70px">
            </telerik:RadToolBarButton>
            <telerik:RadToolBarButton runat="server" CommandName="PDF" Text="PDF" ToolTip="Export the report to PDF" Width="70px">
            </telerik:RadToolBarButton>
            <telerik:RadToolBarButton runat="server" CommandName="Text" Text="TeAdminLogin.aspxxt" ToolTip="Export the report to tab-delimited text" Width="70px">
            </telerik:RadToolBarButton>
        </Items>
    </telerik:RadToolBar>
    </div></td>
            <td valign="top" style="height: 10px">
                </td>
        </tr>
        <tr>
            <td style="width: 550px">
                <telerik:radgrid id="RadGrid1" runat="server" autogeneratecolumns="False" datasourceid="SqlDataSource1"
                    gridlines="None" skin="Black" width="700px" AllowPaging="True" 
                    PageSize="40">
                    <ExportSettings IgnorePaging="True" FileName="AFDDownloadLog">
                        <Pdf FontType="Link" PageLeftMargin="0.3in" PageRightMargin="0.3in" 
                            PageTitle="AFD Downloads Log" />
                    </ExportSettings>
<MasterTableView DataSourceID="SqlDataSource1" GridLines="None">
   
   
   
 
<Columns>
    <telerik:GridBoundColumn DataField="RequestKey" DataType="System.Int32"
        HeaderText="RequestKey" ReadOnly="True" SortExpression="RequestKey" UniqueName="RequestKey"
        Visible="False">
    </telerik:GridBoundColumn>
    <telerik:GridBoundColumn DataField="Request Date" HeaderText="Request Date"
        ReadOnly="True" SortExpression="Request Date" UniqueName="RequestDate">
        <HeaderStyle HorizontalAlign="Left" Width="70px" />
        <ItemStyle HorizontalAlign="Left" Width="70px" />
    </telerik:GridBoundColumn>
        <telerik:GridBoundColumn DataField="SquadronID" HeaderText="Squadron"
        SortExpression="SquadronID" UniqueName="SquadronID">
            <HeaderStyle HorizontalAlign="Left" Width="70px" />
            <ItemStyle HorizontalAlign="Left" Width="70px" />
    </telerik:GridBoundColumn>
    <telerik:GridBoundColumn DataField="List" HeaderText="List"
        SortExpression="List" UniqueName="List">
        <HeaderStyle HorizontalAlign="Left" Width="170px" />
        <ItemStyle HorizontalAlign="Left" Width="170px" />
    </telerik:GridBoundColumn>
    <telerik:GridBoundColumn DataField="ZipCount" DataType="System.Int32"
        HeaderText="Number of Zips" SortExpression="ZipCount" 
        UniqueName="ZipCount" DataFormatString="{0:n0}">
        <HeaderStyle HorizontalAlign="Right" Width="80px" />
        <ItemStyle HorizontalAlign="Right" Width="80px" />
    </telerik:GridBoundColumn>
    <telerik:GridBoundColumn DataField="Records" DataType="System.Int32"
        HeaderText="Records Downloaded" SortExpression="Records" 
        UniqueName="Records" DataFormatString="{0:n0}">
        <HeaderStyle HorizontalAlign="Right" Width="80px" />
        <ItemStyle HorizontalAlign="Right" Width="80px" />
    </telerik:GridBoundColumn>
     <telerik:GridBoundColumn DataField="EmailCount" DataType="System.Int32"
        HeaderText="Email Count" SortExpression="EmailCount" 
        UniqueName="EmailCount" DataFormatString="{0:n0}">
         <HeaderStyle HorizontalAlign="Right" Width="80px" />
         <ItemStyle HorizontalAlign="Right" Width="80px" />
    </telerik:GridBoundColumn>
</Columns>
</MasterTableView>
                    <ClientSettings AllowDragToGroup="True">
                    </ClientSettings>
</telerik:radgrid>
            </td>
            <td valign="top">
                <br />
                </td>
        </tr>
    </table>
    <br />
    <br />
    <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:AirForceDataConnectionString %>"
        SelectCommand="Select RequestKey, SquadronID,  convert(varchar(10),RequestDate,1) as 'Request Date', List, ZipCount, ResultCount as 'Records', EmailCount from dbo.AFD_QueryLog where RequestDate >= dateadd(year,-1, getdate()) order by RequestDate desc">
    </asp:SqlDataSource>
    <br />
    <br />
    </form>
</asp:Content>

