<%@ Page Language="C#" MasterPageFile="~/AFD.master" AutoEventWireup="true" CodeFile="MailingLog.aspx.cs" Inherits="DataStatus" Title="Data Download Log" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<asp:Content ID="Content1" ContentPlaceHolderID="main" Runat="Server">
    <form id="Form1" runat="server">
    <telerik:RadScriptManager ID="RadScriptManager1" runat="server">
    </telerik:RadScriptManager>
    <br />
    All Email Mailings for last 6 months:<br />
    <br />
    <table style="width: 1020px">
        <tr>
            <td style="width: 700px; height: 10px;" align="right">
                    <div class = "right" style="height: 32px"  ><telerik:RadToolBar ID="RadToolBar1" 
                            runat="server" OnButtonClick="RadToolBar1_ButtonClick"
        Skin="Black" CssClass="right">
        <Items>
            <telerik:RadToolBarButton runat="server" CommandName="Excel" Text="Excel" ToolTip="Export the report to Excel" Width="70px">
            </telerik:RadToolBarButton>
            <telerik:RadToolBarButton runat="server" CommandName="PDF" Text="PDF" ToolTip="Export the report to PDF" Width="70px">
            </telerik:RadToolBarButton>
           
        </Items>
    </telerik:RadToolBar>
    </div></td>
            <td valign="top" style="height: 10px">
                </td>
        </tr>
        <tr>
            <td style="width: 550px">
                <telerik:RadGrid ID="RadGrid1" runat="server" AutoGenerateColumns="False" DataSourceID="SqlDataSource1" GroupPanelPosition="Top">
					<MasterTableView DataKeyNames="AFD_MailingKey" DataSourceID="SqlDataSource1">
						<Columns>
							<telerik:GridBoundColumn DataField="AFD_MailingKey" DataType="System.Int32" FilterControlAltText="Filter AFD_MailingKey column" HeaderText="AFD_MailingKey" ReadOnly="True" SortExpression="AFD_MailingKey" UniqueName="AFD_MailingKey" Visible="False">
							</telerik:GridBoundColumn>
							<telerik:GridBoundColumn DataField="Date" FilterControlAltText="Filter Date column" HeaderText="Date" ReadOnly="True" SortExpression="Date" UniqueName="Date">
							</telerik:GridBoundColumn>
							<telerik:GridBoundColumn DataField="Target" FilterControlAltText="Filter Target column" HeaderText="Target" ReadOnly="True" SortExpression="Target" UniqueName="Target">
							</telerik:GridBoundColumn>
							<telerik:GridBoundColumn DataField="Group" FilterControlAltText="Filter Group column" HeaderText="Group" SortExpression="Group" UniqueName="Group">
							</telerik:GridBoundColumn>
							<telerik:GridBoundColumn DataField="Recruiter" FilterControlAltText="Filter Recruiter column" HeaderText="Recruiter" ReadOnly="True" SortExpression="Recruiter" UniqueName="Recruiter">
							</telerik:GridBoundColumn>
							<telerik:GridBoundColumn DataField="Quantity" DataType="System.Int32" FilterControlAltText="Filter Quantity column" HeaderText="Quantity" SortExpression="Quantity" UniqueName="Quantity">
							</telerik:GridBoundColumn>
						</Columns>
					</MasterTableView>
				</telerik:RadGrid>
            </td>
            <td valign="top">
                <br />
                </td>
        </tr>
    </table>
    <br />
    <br />
    <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:AirForceDataConnectionString %>"
        SelectCommand="AFDsp_Report_MailingListforAdmins" SelectCommandType="StoredProcedure">
    </asp:SqlDataSource>
    <br />
    <br />
    </form>
</asp:Content>

