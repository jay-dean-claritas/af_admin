<%@ Control Language="C#" AutoEventWireup="true" CodeFile="MainNavMenu.ascx.cs" Inherits="WebUserControl" %>

   <%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<telerik:RadMenu ID="mnuMain" runat="server" Width="100%" BorderStyle="None" Height="40px" BorderColor="White" BorderWidth="0px">
            <Items>
                <telerik:RadMenuItem runat="server" Text="Home" Width="80px" NavigateUrl="~/Default.aspx" Height="40px" BorderStyle="None" BorderWidth="0px">
                </telerik:RadMenuItem>
                <telerik:RadMenuItem runat="server" Text="Reports" Width="80px" Height="40px">
                    <Items>
                        <telerik:RadMenuItem runat="server" Text="Quick Report" NavigateUrl="~/QuickReport2.aspx">
                        </telerik:RadMenuItem>
                        <telerik:RadMenuItem runat="server" Text="Quick Report - CST" NavigateUrl="~/QuickReport2_CST.aspx">
                        </telerik:RadMenuItem>
                        <telerik:RadMenuItem runat="server" Text="Launch Report" NavigateUrl="~/Reports/LaunchReport2.aspx">
                        </telerik:RadMenuItem>
                        <telerik:RadMenuItem runat="server" Text="Process Reports">
                            <Items>
                                <telerik:RadMenuItem runat="server" NavigateUrl="~/Reports/Process/CheetahBouncesLog.aspx"
                                    Text="Cheetah Uploads">
                                </telerik:RadMenuItem>
                                <telerik:RadMenuItem runat="server" NavigateUrl="~/Reports/Process/HardBouncesUploadFromMSLog.aspx"
                                    Text="MS Hard Bounces">
                                </telerik:RadMenuItem>
                                <telerik:RadMenuItem runat="server" NavigateUrl="../Reports/Process/AdditionsToDNM.aspx"
                                    Text="DNM Additions">
                                </telerik:RadMenuItem>
                                <telerik:RadMenuItem runat="server" NavigateUrl="../Reports/Process/LatestMSTRacking.aspx"
                                    Text="Latest MS Tracking Activity">
                                </telerik:RadMenuItem>
                                <telerik:RadMenuItem runat="server" NavigateUrl="~/Reports/Process/MSActivityChart.aspx"
                                    Text="MS Activity by Hour">
                                </telerik:RadMenuItem>
                            </Items>
                        </telerik:RadMenuItem>
                        <telerik:RadMenuItem runat="server" Text="Bounce Reports">
                            <Items>
                                <telerik:RadMenuItem runat="server" NavigateUrl="~/Reports/BounceReport_MS.aspx"
                                    Text="Bounces From MS">
                                </telerik:RadMenuItem>
                                <telerik:RadMenuItem runat="server" NavigateUrl="~/Reports/BounceReport_SM.aspx"
                                    Text="Bounces From SM">
                                </telerik:RadMenuItem>
                                <telerik:RadMenuItem runat="server" NavigateUrl="~/Reports/BounceReportbyDomain.aspx"
                                    Text="Domain Bounces">
                                </telerik:RadMenuItem>
                            </Items>
                        </telerik:RadMenuItem>
                    </Items>
                </telerik:RadMenuItem>
                <telerik:RadMenuItem runat="server" Text="List Managers" Width="125px" Height="40px">
                    <Items>
                        <telerik:RadMenuItem runat="server" Text="Client Names for OptOut" NavigateUrl="~/tableEditor.aspx">
                        </telerik:RadMenuItem>
                        <telerik:RadMenuItem runat="server" Text="Master Client List" NavigateUrl="~/TableEditors/Clients_Table.aspx" >
                        </telerik:RadMenuItem>
                        <telerik:RadMenuItem runat="server" Text="Agency / Reseller List" NavigateUrl="../TableEditors/Agency_Table.aspx">
                        </telerik:RadMenuItem>
                        <telerik:RadMenuItem runat="server" Text="Client Contact List" NavigateUrl="../TableEditors/ClientContacts_Table.aspx" >
                        </telerik:RadMenuItem>
                        <telerik:RadMenuItem runat="server" Text="Agency Contact List" NavigateUrl="../TableEditors/AgencyContacts_Table.aspx">
                        </telerik:RadMenuItem>
                        <telerik:RadMenuItem runat="server" NavigateUrl="~/TableEditors/MidasMergeVars_Table.aspx"
                            Text="Midas Merge Variables">
                        </telerik:RadMenuItem>
                    </Items>
                </telerik:RadMenuItem>
                <telerik:RadMenuItem runat="server" Text="Calendars" Height="40px">
                </telerik:RadMenuItem>
                <telerik:RadMenuItem runat="server" Text="Utilities" Width="80px" Height="40px">
                    <Items>
                        <telerik:RadMenuItem runat="server" Text="Supression Tools">
                        </telerik:RadMenuItem>
                    </Items>
                </telerik:RadMenuItem>
            </Items>
        </telerik:RadMenu>

