﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class AddAdminUser : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

        string role = Session["CurrentUserRole"].ToString();

        if (role != "SuperAdmin")
        {
            Response.Redirect("DataSelect_Admin.aspx");
        }

    }

    protected void btnSave_Click(object sender, EventArgs e)
    {

        string UserID = this.txtUserID.Text;
        string PWD = this.txtPassword.Text;
        

        AdminUser newAdmin = new AdminUser(UserID, PWD);


        int result = newAdmin.AddNewAdmin(this.txtUserName.Text, this.txtUserEmail.Text, "Admin");





    }
    protected void btnResetPwd_Click(object sender, EventArgs e)
    {

        AdminUser CurrUser = new AdminUser(Session["CurrentUserID"].ToString(), this.txtResetMyPassword.Text);

        if (CurrUser.Authenticate() == false)
        {
            this.lblBadPassword.Visible = true;
            this.lblResetSuccess.Visible = false;
            this.lblResetNoUserName.Visible = false;


            this.txtResetMyPassword.Focus();
            return;
        }


        int result = CurrUser.ResetPWD(this.txtResetUserName.Text, this.txtResetNewPassword.Text);

        if (result == 0)
        {
            this.lblResetNoUserName.Visible = true;
            this.lblBadPassword.Visible = false;
            this.lblResetSuccess.Visible = false;
            this.txtResetUserName.Focus();

        }
        else
        {
            this.lblResetSuccess.Visible = true;
            this.lblResetNoUserName.Visible = false;
            this.lblBadPassword.Visible = false;

        }


    }
}
