﻿<%@ Page Title="" Language="C#" MasterPageFile="~/AFD.master" AutoEventWireup="true" CodeFile="AddAdminUser.aspx.cs" Inherits="AddAdminUser" %>

<asp:Content ID="Content1" ContentPlaceHolderID="main" Runat="Server">


    <form id="form1" runat="server">


    <br />
    Add a new Admin User:<br />
   
        <table style="width:750px;">
            <tr>
                <td style="width: 127px">
                    User Name:</td>
                <td style="width: 293px">
                    <asp:TextBox ID="txtUserName" runat="server" 
                         Width="222px"></asp:TextBox>
                </td>
                <td align="right" style="width: 175px">
                    UserID:</td>
                <td>
                    <asp:TextBox ID="txtUserID" runat="server"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td style="width: 127px">
                    Email:</td>
                <td style="width: 293px">
                    <asp:TextBox ID="txtUserEmail" runat="server" 
                         Width="222px"></asp:TextBox>
                </td>
                <td align="right" style="width: 175px">
                    Password:</td>
                <td>
                    <asp:TextBox ID="txtPassword" runat="server"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td style="width: 127px">
                    &nbsp;</td>
                <td style="width: 293px">
                    &nbsp;</td>
                <td style="width: 175px">
                    &nbsp;</td>
                <td>
                    &nbsp;</td>
            </tr>
            <tr>
                <td style="width: 127px">
                    &nbsp;</td>
                <td style="width: 293px">
                    &nbsp;</td>
                <td style="width: 175px">
                    <asp:Button ID="btnSave" runat="server" onclick="btnSave_Click" Text="Save" 
                        Width="79px" />
                </td>
                <td>
                    &nbsp;</td>
            </tr>
        </table>
    


    <br />
    <br />
    Reset Password for a User<br />
    <table style="width: 700px;">
        <tr>
            <td style="width: 135px; height: 46px">
                My Password:</td>
            <td style="height: 46px; width: 303px">
                    <asp:TextBox ID="txtResetMyPassword" runat="server"></asp:TextBox>
                </td>
            <td style="height: 46px">
                <asp:Label ID="lblBadPassword" runat="server" ForeColor="#FF6666" 
                    Text="**Incorrect Password" Visible="False"></asp:Label>
            </td>
        </tr>
        <tr>
            <td style="width: 135px; height: 46px">
                User Name:</td>
            <td style="height: 46px; width: 303px">
                    <asp:TextBox ID="txtResetUserName" runat="server" 
                         Width="222px"></asp:TextBox>
                </td>
            <td style="height: 46px">
                <asp:Label ID="lblResetNoUserName" runat="server" ForeColor="#FF6666" 
                    Text="**This username not found" Visible="False"></asp:Label>
            </td>
        </tr>
        <tr>
            <td style="width: 135px; height: 47px">
                New Password:</td>
            <td style="height: 47px; width: 303px">
                    <asp:TextBox ID="txtResetNewPassword" runat="server"></asp:TextBox>
                </td>
            <td style="height: 47px">
                <asp:Label ID="lblResetSuccess" runat="server" Text="Password Updated!" 
                    Visible="False"></asp:Label>
            </td>
        </tr>
        <tr>
            <td style="width: 135px; height: 28px">
            </td>
            <td align="right" style="height: 28px; width: 303px">
                <asp:Button ID="btnResetPwd" runat="server" onclick="btnResetPwd_Click" 
                    Text="Reset" Width="94px" />
                <br />
            </td>
            <td style="height: 28px">
            </td>
        </tr>
    </table>
    <br />
    <br />
    


    </form>
    


</asp:Content>

