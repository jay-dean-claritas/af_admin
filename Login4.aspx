<%@ Page Language="C#" %>
<%--<%@ Import Namespace="System.Configuration.ConfigurationManager" %>--%>


<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<script runat="server">

    protected void Page_Load(object sender, EventArgs e)
    {   
        string sqdrn = string.Empty;
        string code = string.Empty;
        string referrer = string.Empty;
        string IPAddress = string.Empty;
 
        //Session.Abandon();
        
        if (Request.QueryString.Count > 0)
        {
            sqdrn = Convert.ToString(Context.Request.QueryString["sqd"]);
            code = Convert.ToString(Context.Request.QueryString["cd"]);

            if (sqdrn == null) { sqdrn = Convert.ToString(Context.Request.QueryString["SQD"]); }
            if (code == null) { code = Convert.ToString(Context.Request.QueryString["CD"]); }
        }
        else if (Request.Form.Count > 0) 
        {
            sqdrn = Convert.ToString(Context.Request.Form["sqd"]);
            code = Convert.ToString(Context.Request.Form["cd"]);

            if (sqdrn == null) { sqdrn = Convert.ToString(Context.Request.Form["SQD"]); }
            if (code == null) { code = Convert.ToString(Context.Request.Form["CD"]); }
        }

        if (Context.Request.UrlReferrer != null)
        {

            referrer = Context.Request.UrlReferrer.ToString();
            IPAddress = Context.Request.UserHostAddress.ToString();
        }
        
        
        string message =  " SQD:" + sqdrn + " Referrer:" + referrer + " IP:" + IPAddress;
        WriteToLogFile(message); 
       
        
        //if the credentials are not there, display the logged out info
        if ((sqdrn == string.Empty) | (code == string.Empty)| (code == null))
        {
            //Redirect back to AirForceAds?
            Response.Redirect("http://airforceads.com");
        } 
        else 
        {
            Squadron Squad = new Squadron(sqdrn, code);
           
            if (Squad.Validate()) 
            {
                FormsAuthentication.SetAuthCookie(sqdrn, false);

                Session["CurrentSqdrn"] = sqdrn;
                
                
                
                Response.Redirect("dataselect.aspx");
            } else {
 
               Response.Redirect("http://airforceads.com");
            }
        }
        
        
        
    }



    protected void Login1_Authenticate(object sender, AuthenticateEventArgs e)
    {



        string sqdrn = this.Login1.UserName;
        string code = this.Login1.Password;




        Squadron Squad = new Squadron(sqdrn, code);
        if (Squad.Validate())
        {
            FormsAuthentication.SetAuthCookie(sqdrn, false);
            
            Session["CurrentSqdrn"] = sqdrn;

            Response.Redirect("dataselect.aspx");
        }
        else
        {

            Response.Redirect("http://airforceads.com");
        }
        
        
        

    }

    private int WriteToLogFile(string message)
    {
        string logfilepathandname;


        //Get from settings file
        logfilepathandname = System.Web.Configuration.WebConfigurationManager.AppSettings["accesslogpathandname"];

        System.IO.StreamWriter sw = System.IO.File.AppendText(logfilepathandname); // Define the logfile

        try
        {

            string logLine = System.String.Format("{0:G}| {1}.", System.DateTime.Now, message);
            sw.WriteLine(logLine);
        }
        catch
        {
            return -1;
        }
        finally
        {
            sw.Close();
        }

        return 1;

    }
</script>

<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">
    <title>Air Force Data Login</title>
    <link rel="shortcut icon" href="App_Themes/Theme1/favicon.ico" />

<style type="text/css">


    body
{
		
	background: #003d4c;
	font-family:'lucida grande',verdana,helvetica,arial,sans-serif;
	font-size:90%;
	color: white;
	margin: 0;
	background: url(../img/background.jpg) #000 no-repeat center top;
	background-attachment: fixed;
}
    
    
    
</style>


</head>
<body>
    <form id="AFD_LI" runat="server">
   <div id="header">
	<div>
        <img id="imgLogo"  alt="Air Force Ads" class="left" src="./App_Themes/Theme1/airforceadscom-logo.png" />&nbsp;&nbsp;
        <img id="imgLogo2"  alt="Air Force Ads"  class="right" src="App_Themes/Theme1/logo.png" /><br />
        <br />
        <br />
        <br />
        This site cannot be accessed directly. Please connect to
        <br />
        AIRFORCEADS.com and reconnect to this site using the links there.<br />
        <br />
        <br />
        <asp:Login ID="Login1" runat="server" DisplayRememberMe="False" PasswordLabelText="Access Code:" OnAuthenticate="Login1_Authenticate">
        </asp:Login>
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
    </div>
    </div>

   <div id="content-container">
  <div id="content_onecol" > <a id="maincontent"></a>
     
       
  </div><%--end of the MainContent div--%>
  

<div class="clear"></div>

</div>
<!--   END CONTENT CONTAINER  -->

<!--   FOOTER   -->
<div id="footer">
    <div class="indent">
      <p><img id="Image1" alt="AcquireWeb" class="right"
         src="App_Themes/Theme1/airforceadscom-logo.png" /></p>
    </div>
</div> 
  </form>
</body>
</html>
