using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Telerik.Web.UI;


public partial class DataStatus : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }





    protected void btnExcel_Click(object sender, EventArgs e)
    {


        ExportToExcel();
    }

    protected void ExportToExcel()
    {
        string filename = "AFDDownLoadReport_" + DateTime.Now.ToString("yyyyMMdd-hhmm");
        this.RadGrid1.ExportSettings.FileName = filename;
        this.RadGrid1.MasterTableView.ExportToExcel();
    }

    protected void ExportToPDF()

    {
        //string filename = "AFDDownLoadReport_" + DateTime.Now.ToString("yyyyMMdd-hhmm") + ".pdf";
        //this.RadGrid1.ExportSettings.FileName = filename;
        this.RadGrid1.MasterTableView.ExportToPdf();
    }





   
    protected void btnPrint_Click(object sender, EventArgs e)
    {
        ExportToPDF();
    }
    protected void RadToolBar1_ButtonClick(object sender, Telerik.Web.UI.RadToolBarEventArgs e)
    {

        Telerik.Web.UI.RadToolBarButton btn = e.Item as Telerik.Web.UI.RadToolBarButton;

        


        switch (btn.CommandName)
        {
            
            case "Excel":
                ExportToExcel();
                break;
            case "PDF":
                ApplyStylesToPDFExport(this.RadGrid1.MasterTableView);
                ExportToPDF();
                break;
            case "Text":
                //add the Text Export code
                break;
        }


    }

    protected void ApplyStylesToPDFExport(GridTableView tableview)
    {
        //string title = "AW Quick Report for " + DateTime.Now.ToString("f");
        //this.grdQuickReport.ExportSettings.Pdf.PageTitle = title;
        //this.lblTableName.Text = title;

        this.RadGrid1.AllowPaging = false;


        GridItem Header = tableview.GetItems(GridItemType.Header)[0];
        Header.Style["font-size"] = "8pt";
        Header.Style["color"] = "white";
        Header.Style["background-color"] = "#777";
        Header.Style["height"] = "20px";
        Header.Style["verticle-align"] = "middle";

        

        foreach (TableCell cell in Header.Cells)
        {
            cell.Style["text-align"] = "center";
            cell.Style["font-weight"] = "bold";
        }

        foreach (GridColumn Col in tableview.Columns)
        {
            Col.ItemStyle.HorizontalAlign = System.Web.UI.WebControls.HorizontalAlign.Right;
        }



        //Loop through the data items
        foreach (GridItem dataitem in tableview.Items)
        {
            dataitem.Style["font-size"] = "6pt";
            dataitem.HorizontalAlign = System.Web.UI.WebControls.HorizontalAlign.Right;

            if (dataitem.ItemType == GridItemType.AlternatingItem)
            {
                dataitem.Style["background-color"] = "#D3D3D3";//#D3D3D3  (LightGrey)
            }


        }


        //GridColumn col = tableview.Columns[0];
        //col.ItemStyle.Width = System.Web.UI.WebControls.Unit.Pixel(150);


    }
}
